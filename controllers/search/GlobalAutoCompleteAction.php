<?php
class GlobalAutoCompleteAction extends CAction
{
    public function run($filter = null){
        $searchP = $_POST;	
        if(@$_GET["name"])
        	$searchP = $_GET;
        
        $res = Search::globalAutoComplete($searchP, $filter);
        
        if(@$_POST['tpl'])
            echo $this->getController()->renderPartial($_POST['tpl'], array("result"=>$res));
        else if(@$_GET['jqAuto']){
        	$resJQ = array();
        	foreach ($res["results"] as $k => $v) {
        		$resJQ[] = array(
        			"id" => $k,
        			"label" => (@$v['name']) ? $v['name'] : "noName",
        			"type" => @$v['type'],
        			"value" => (@$v['name']) ? $v['name'] : "noName",
                    "slug" => @$v['slug'],
                    "img" => @$v['profilThumbImageUrl'],
        		);
        	}
        	Rest::json($resJQ);
        }
        else
            Rest::json($res);

        Yii::app()->end();
    }
}